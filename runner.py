# Windowsでpdf2imageを走らせたい場合、Poppler for windowsが別途必要。
# https://github.com/Belval/pdf2image
# http://blog.alivate.com.au/poppler-windows/
# Tesseractもインストールしないといけない。OCRできないよ！！
# https://github.com/tesseract-ocr/tesseract
# https://qiita.com/henjiganai/items/7a5e871f652b32b41a18

import sys
import os
from pathlib import Path
from src.application_service import ApplicationService


if __name__ == '__main__':
    # 各種ディレクトリ。環境変数に書き込む。
    input_dir = Path(sys.argv[0]).parent / "input"
    working_dir = Path(sys.argv[0]).parent / "working"
    output_dir = Path(sys.argv[0]).parent / "output"
    log_dir = Path(sys.argv[0]).parent / "log"
    env_path = Path(sys.argv[0]).parent / ".env"
    os.environ["INPUT_DIR"] = str(input_dir)
    os.environ["WORKING_DIR"] = str(working_dir)
    os.environ["OUTPUT_DIR"] = str(output_dir)
    os.environ["LOG_DIR"] = str(log_dir)
    os.environ["ENV_PATH"] = str(env_path)

    # そろそろモジュールで割ったほうがいいかもしれない。
    # ドメインモデル・アプリケーションサービスを導入するか。
    app = ApplicationService("reception-extractor")
    app.extract()
