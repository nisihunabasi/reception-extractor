import sys
import os
from dotenv import load_dotenv
from src.domains import *
from src.infrastructure import TextRepository


class ApplicationService:
    def __init__(self, logger_name: str):
        # 環境変数の読み込み。中に定義されている（はずの）各種外部アプリパスを格納。
        load_dotenv(os.environ["ENV_PATH"])
        self.logger = self.get_logger(logger_name)

    def extract(self):
        """
        PDFで出来た受付帳から、データを抽出するアクション。
        :return:
        """
        # ファイルパスを調べる。
        input_dir = Path(os.environ["INPUT_DIR"])
        file_list = list(input_dir.glob("*.pdf"))

        # 処理した日時を記録。各出力ファイル名に加える。
        now = datetime.now()
        timestamp = now.strftime("%Y%m%d%H%M%S")

        ocr = Ocr(self.logger, timestamp)
        # TODO 受け入れの幅を広げる。URL直接指定とか。
        if len(file_list) >= 1:
            self.logger.info("PDFを画像に変換します。")
            cur_working_dir = ocr.import_pdf_and_export_jpg(file_list[0])
            self.logger.info("画像変換完了。OCRを実行します。")
            ocr.execute_ocr(cur_working_dir)
        else:
            self.logger.error("処理を行うPDFが見当たりません。inputディレクトリにPDFファイルを入れてください。")
            return False

        # データのパースを行う。
        inter_result = TextRepository.get(ocr.get_output_file_path())
        self.logger.info("OCR結果を元に、データをパースします。")
        book = ReceptionBook(inter_result)
        book.execute_preprocess()
        book.parse()
        book.correct_notation()

        output_path = Path(os.environ["OUTPUT_DIR"]) / ("output_" + timestamp + ".csv")
        TextRepository.save(output_path, book.entries)

        self.logger.info("すべての処理が終了しました。出力データのファイル名は、" + output_path.name + "です。")

    def get_logger(self, name):
        logger = py_logging.getLogger(name)

        handler = py_logging.StreamHandler()
        handler.setLevel(py_logging.DEBUG)

        file_path = os.environ["LOG_DIR"] + os.sep + name + ".log"

        file_handler = py_logging.FileHandler(filename=file_path, encoding="utf-8")
        formatter = py_logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        file_handler.setLevel(py_logging.INFO)

        logger.setLevel(py_logging.DEBUG)
        logger.addHandler(handler)
        logger.addHandler(file_handler)

        return logger
