from pathlib import Path


class TextRepository:
    @staticmethod
    def get(path: Path) -> list:
        result = []
        with open(str(path), encoding="UTF-8") as fp:
            line = fp.readline()
            while line:
                result.append(line)
                line = fp.readline()

        return result

    @staticmethod
    def save(path: Path, l: list, line_suffix="\n") -> None:
        if line_suffix:
            input_l = [r + line_suffix for r in l]
        with open(str(path), encoding="UTF-8", mode="w") as fp:
            fp.writelines(input_l)