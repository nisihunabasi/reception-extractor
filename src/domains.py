from typing import List
from pathlib import Path
import re
import os
import sys
import logging as py_logging
from datetime import datetime
from pdf2image import convert_from_path
import subprocess
from logging import Logger


class ReceptionBook:
    source_lines: List[str]
    entries: List[str]

    def __init__(self, source_lines: List):
        # ファイルを開いて格納する。
        self.source_lines = source_lines

    def parse(self):
        result = []
        # 値の配置は、id, accept_date, accept_attr, reason, label, jimoku, address
        mem = ["", "", "", "", "", "", ""]
        read_first_row = False
        read_second_row = False
        for row in self.source_lines:
            # 情報1個分のデータが揃ったらresultに格納する。
            if read_first_row and read_second_row:
                result.append(",".join(mem))
                read_first_row = False
                read_second_row = False
                mem = ["", "", "", "", "", "", ""]

            # 何が書かれている行であるか、検知する。
            # 行頭が墨付きカッコなら1行目
            # 行頭に「既)」や「新)」なら2行目
            m = re.match(r"^【", row)
            if m is not None:
                tmp_info = self.__parse_first_row(row)
                mem[0] = tmp_info[0]
                mem[1] = tmp_info[1]
                mem[2] = tmp_info[2]
                mem[3] = tmp_info[3]
                read_first_row = True
                continue

            m = re.match(r".[)）]", row)
            if m is not None:
                tmp_info = self.__parse_second_row(row)
                mem[4] = tmp_info[0]
                mem[5] = tmp_info[1]
                mem[6] = tmp_info[2]
                #mem[3] = tmp_info[3]
                read_second_row = True
                continue

        # 最後のエントリーの挿入。データが完結しているなら挿入する。
        if read_first_row and read_second_row:
            result.append(",".join(mem))

        self.entries = result

    def __parse_first_row(self, row: str):
        """
        1行目をパースする。
        :param row:
        :return:
        """
        m = re.search(r"[(（{【\[].*?([0-9]+).*?[)）}】\]]([\d\?]{1,2}月[\d\?]{1,2}日).+?[(（](.独|連先|連続)[)）](.+)", row)
        if m is not None:
            return [m.group(1), m.group(2), m.group(3), m.group(4)]
        else:
            return ["", "", "", ""]

    def __parse_second_row(self, row: str):
        m = re.search(r"^(.)[\)）](..)([^ 　\r\n\f]+)", row)
        if m is not None:
            return [m.group(1), m.group(2), m.group(3)]
        else:
            return ["", "", ""]

    def correct_notation(self):
        """
        表記ゆれの修正。entriesに対して行う。修正結果をentriesに代入する。
        :return:void
        """
        if self.entries is None or len(self.entries) == 0:
            return

        for i, entry in enumerate(self.entries):
            tmp_row = entry.split(",")
            # accept_attr
            tmp_row[2] = re.sub(r".独", "単独", tmp_row[2])
            # reason
            tmp_row[3] = re.sub(r"^.失$", "滅失", tmp_row[3])
            tmp_row[3] = re.sub(r"法人合.", "法人合併", tmp_row[3])
            tmp_row[3] = re.sub(r"^分.$", "分筆", tmp_row[3])
            tmp_row[3] = re.sub(r"^合.$", "合筆", tmp_row[3])
            tmp_row[3] = re.sub(r"^表.$", "表題", tmp_row[3])
            tmp_row[3] = re.sub(r"^抹消..$", "抹消登記", tmp_row[3])
            # label
            tmp_row[4] = re.sub(r"肥", "既", tmp_row[4])
            # address
            # この辺は読み間違えを起こしているものを一つづつ潰していくしか無いが、それにも限度があるので、気づいたところだけやる。
            tmp_row[6] = re.sub(r"(\d)[ー一]", r"\1-", tmp_row[6])
            tmp_row[6] = re.sub(r"前.市", r"前橋市", tmp_row[6])

            self.entries[i] = ",".join(tmp_row)

        return

    def execute_preprocess(self):
        """
        source_linesに格納されている生データを、本番処理の前に使いやすい形に前処理する。
        値はまたsource_linesに格納する。
        :return:void
        """
        # 余計なスペース等を取り払う。
        result_lines = []
        for line in self.source_lines:
            # まず空行だけ取り払う。
            tmp_text = re.sub(r"[ 　\r\n\f\t]", r"", line)
            if tmp_text != "":
                # 不要な行を無視する
                m = re.search(r"([<＜く] ?不動産 ?[>＞]|\d{1,2}月 ?\d{1,2}日.+作成|ページ)", line)
                if m is not None:
                    continue
                # 完全に不要なスペースだけ取り払う。意味のあるスペースは残す。
                # ただし、複数スペースが連なっている文字列は、1つ分だけスペースが削られる。効率と負荷のトレードオフとして受け入れる。
                input_text = line.strip(" 　\r\n\f\t")
                input_text = re.sub(r"([^ 　\f\t])[ 　\f\t]", r"\1", input_text)
                result_lines.append(input_text)

        self.source_lines = result_lines
        return


class Ocr:
    tesseract_path: Path
    poppler_path: Path
    __output_file_path: Path

    def __init__(self, logger: Logger, timestamp: str):
        self.tesseract_path = Path(os.environ["TESSERACT_PATH"])
        self.poppler_path = Path(os.environ["POPPLER_PATH"])

        # ロガー等の整備
        self.logger = logger

        self.timestamp = timestamp

        self.input_dir = Path(os.environ["INPUT_DIR"])
        self.working_dir = Path(os.environ["WORKING_DIR"])
        self.output_dir = Path(os.environ["OUTPUT_DIR"])

    def import_pdf_and_export_jpg(self, file_path: Path):
        """
        入力PDFをインポートし、PNGファイルに変換する。
        :param file_path:Path
        :return:Path:画像の保存先のパス。
        """
        # PDF取得、画像化を行う。
        cur_working_dir: Path = self.working_dir / self.timestamp
        if not cur_working_dir.is_dir():
            cur_working_dir.mkdir()

        convert_from_path(str(file_path), poppler_path=str(self.poppler_path), dpi=400, fmt="jpeg",
                                   output_folder=cur_working_dir)

        # 縦方向の画像であると仮定して進める。
        return cur_working_dir

    def execute_ocr(self, images_dir: Path):
        """
        OCRを実行する。
        ソースとなる画像を元に、読みだした結果を出力先のファイルに保存する。
        :param images_dir:Path
        :return:None
        """
        # TODO 一時ファイルが消せないなら、workingにファイルを作ったほうが健全では？
        # 入力ファイルパス一覧を作成。
        file_list = list(images_dir.glob("*.jpg"))
        input_file_path = self.working_dir / ("input_path_" + self.timestamp + ".txt")
        with open(str(input_file_path), "w+b") as fp:
            fp.write(bytes("\n".join([str(n) for n in file_list]), "utf-8"))

        # 出力ファイルのパス。workingに格納する。
        self.__output_file_path = self.working_dir / ("result_" + self.timestamp)
        # 実際の処理
        subprocess.call(
            str(self.tesseract_path) + " " + str(input_file_path) + " " + str(self.__output_file_path)
            + " -l jpn --oem 1 --psm 4")
        # Tesseractのoutput_pathは、なぜか拡張子を自分で付け加えようとする。なので、こちらが前もって拡張子を振ると、それにかぶせて付けてくる。
        # その仕様に合わせるために、コマンド実行した後に拡張子を付け加える。txtなのでWindows前提になってしまうが・・・
        self.__output_file_path = Path(str(self.__output_file_path) + ".txt")

    def get_output_file_path(self):
        """
        最終的なOCR結果を記載したテキストファイルのパスを返す。
        :return:Path
        """
        return self.__output_file_path
