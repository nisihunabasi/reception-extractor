# reception-extractor

不動産登記受付帳のスキャンデータをOCRで読み取り、正規化して再利用可能なテキストデータに仕立てるアプリケーションです。  
参考URL  
https://re-touki.com/wordpress/touki

## インストール

Windows10にて動作確認しています。多分Mac/Linuxでは動きません・・・  
別途、TesseractとPoppler for Windowsが必要です。予めインストールしてください。  
Tesseract  
https://github.com/tesseract-ocr/tesseract
https://qiita.com/henjiganai/items/7a5e871f652b32b41a18  
Poppler for Windows  
http://blog.alivate.com.au/poppler-windows/  

Pythonの実行環境も必要です。Python3.7以降をインストールしてください。  
https://www.python.jp/

pipenvのインストールも必要です。。。だんだん説明めんどくさくなってきました(´・ω・｀)

git clone あたりでこのアプリをダウンロードしてください。  
その後、pipenv install すると、必要なパッケージが読み込まれます。

ルートディレクトリで、.envを作成します。.env.exampleをコピーして、その中にある各種アプリのパスを自分の環境に合わせて書き換えてください。  
envではTesseractとPopplerのパスを書いてください。

## 使い方

inputディレクトリに、読み込みたいPDFファイルを配置します。できれば1バイト文字のファイル名にしてください。

コマンドラインでカレントディレクトリをアプリケーションのディレクトリに合わせ、
<pre>
pipenv shell
python runner.py
</pre>
と打ち込みます。  
完了すると、outputディレクトリにテキストファイルが生成されます。  
100ページのPDFファイルで10分ほどかかるかと思いますので、気長に待ってください。

