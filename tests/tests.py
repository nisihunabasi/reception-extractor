import unittest
from pathlib import Path
from src.domains import ReceptionBook


class Tests(unittest.TestCase):
    def test_parsing(self):
        path = Path("test_data/test_data_short.txt")
        book = ReceptionBook(path)

        # 前処理する
        book.execute_preprocess()
        actual_lines = book.source_lines
        expected_lines = [
            "【第6236号       】4月2日受付(単独)共同担保変更通知",
            "既)共担5006",
            "【第6237号       】4月2日受付(単独)共同担保変更通知",
            "既)共担8757",
            "【第6238号       】4月2日受付(単独)減失",
            "既)建物前橋市南町3丁目55ー2-1"
        ]
        self.assertEqual(len(actual_lines), len(expected_lines))
        for i, line in enumerate(actual_lines):
            self.assertEqual(line, expected_lines[i])


        # 本処理のパースする
        book.parse()
        # 値の配置は、id, accept_date, accept_attr, reason, label, jimoku, address
        actual_data = book.entries
        expected_data = [
            "6236,4月2日,単独,共同担保変更通知,既,共担,5006",
            "6237,4月2日,単独,共同担保変更通知,既,共担,8757",
            "6238,4月2日,単独,減失,既,建物,前橋市南町3丁目55ー2-1"
        ]

        self.assertEqual(len(actual_data), len(expected_data))
        for i, entry in enumerate(actual_data):
            self.assertEqual(entry, expected_data[i])

        # 文字の正規化を行う。
        book.correct_notation()
        actual_data = book.entries
